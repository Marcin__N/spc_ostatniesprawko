clear;
close;
grid on;

Ts=1;
num1 = [ 1 ];
den1 = [ 1 -1 ];
H1 = tf(num1,den1,Ts);

num2 = [ 1  0 ];
den2 = [ 1  -1 ];
H2 = tf(num2,den2,Ts);

num3 = [ 1  0 ];
den3 = [ 1  -2  1 ];
H3 = tf(num3,den3,Ts);

num4 = [ 2  1  0 ];
den4 = [ 1  1  -6 ];
H4 = tf(num4,den4,Ts);


num5 = [ 0  0  7  6 ];
den5 = [ 2  1  4  5 ];
H5 = tf(num5,den5, Ts);

%dstep(num0, den0)
%dimpulse(num1, den1)
%dbode(num, den, 1)
%dnyquist(num4, den4, 1)

% Models=[H1, H2, H3, H4, H5];
% titles={'System1','System2','System3','System4','System5'}
% 
% for i=1:size(Models,2)
%     figure('Name',char(titles(i)),'NumberTitle','off')
%     [X, T] = step(Models(i));
%     subplot(2,1,1);
%     plot(T, X);
%     title('odpowiedz skokowa');
%     [X, T] = impulse(Models(i));
%     subplot(2,1,2);
%     plot(T, X);
%     title('odpowiedz impulsowa');
%     print(char(titles(i)),'-dpng')
% end

%%%%%%%%%%skokowa

% figure('Name','System1');
% step(H1);
% hold on;
% print('System1Step','-dpng')
% figure('Name','System2');
% step(H2);
% hold on;
% print('System2Step','-dpng')
figure('Name','System3');
step(H3);
hold on;
print('System3Step','-dpng')
figure('Name','System4');
step(H4);
hold on;
print('System4Step','-dpng')
figure('Name','System5');
step(H5);
hold on;
print('System5Step','-dpng')

% %%%%%%%%%%impulsowa
figure('Name','System1');
impulse(H1);
hold on;
print('System1Impulse','-dpng')
figure('Name','System2');
impulse(H2);
hold on;
print('System2Impulse','-dpng')
% figure('Name','System3');
% impulse(H3);
% hold on;
% print('System3Impulse','-dpng')
% figure('Name','System4');
impulse(H4);
hold on;
print('System4Impulse','-dpng')
figure('Name','System5');
impulse(H5);
hold on;
print('System5Impulse','-dpng')
% 
% % %%%%%%%%%%bode
% figure('Name','System1');
% bode(H1);
% hold on;
% print('System1bode','-dpng')
% figure('Name','System2');
% bode(H2);
% hold on;
% print('System2bode','-dpng')
% figure('Name','System3');
% bode(H3);
% hold on;
% print('System3bode','-dpng')
% figure('Name','System4');
% bode(H4);
% hold on;
% print('System4bode','-dpng')
% figure('Name','System5');
% bode(H5);
% hold on;
% print('System5bode','-dpng') 
% 
% % %%%%%%%%%nyquist
% figure('Name','System1');
% nyquist(H1);
% hold on;
% print('System1nyquist','-dpng')
% figure('Name','System2');
% nyquist(H2);
% hold on;
% print('System2nyquist','-dpng')
% figure('Name','System3');
% nyquist(H3);
% hold on;
% print('System3nyquist','-dpng')
% figure('Name','System4');
% nyquist(H4);
% hold on;
% print('System4nyquist','-dpng')
% figure('Name','System5');
% nyquist(H5);
% hold on;
% print('System5nyquist','-dpng') 
% 
% % %%%%%%%zera i bieguny dla Nyquista
% figure('Name','System1');
% pzmap(H1);
% hold on;
% print('System1pzmap','-dpng')
% figure('Name','System2');
% pzmap(H2);
% hold on;
% print('System2pzmap','-dpng')
% figure('Name','System3');
% pzmap(H3);
% hold on;
% print('System3pzmap','-dpng')
% figure('Name','System4');
% pzmap(H4);
% hold on;
% print('System4pzmap','-dpng')
% figure('Name','System5');
% pzmap(H5);
% hold on;
% print('System5pzmap','-dpng') 

