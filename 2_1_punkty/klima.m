clear all;
close all;

Amp = 0;
Base = 35;
Freq = 0.05;
step_time = 100;

Ri = [1,10,100]; 
Ci = [1,10,100];

for i=1:length(Ri)
    Rp = Ri(i);
    C = Ci(1);
    X0 = 35;
    
    Ue = 35;
    Io = 15;

    sim('klim',1000);
    figure(1);
    hold on;
    plot(T,X);
    legend('R = 1', 'R=10', 'R=100');
    figure(2);
    hold on;
    plot(T, Xr);
    legend('R = 1', 'R=10', 'R=100');
end

for i=1:length(Ri)
    Rp = Ri(1);
    C = Ci(i);
    X0 = 35;
    
    Ue = 35;
    Io = 15;

    sim('klim',1000);
    figure(3);
    hold on;
    plot(T,X);
    legend('C = 1', 'C=10', 'C=100');
    figure(4);
    hold on;
    plot(T, Xr);
    legend('C = 1', 'C=10', 'C=100');
end