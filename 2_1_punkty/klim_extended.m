clear all;
close all;

Amp = 10; % oscylacje temp zew te� sobie dobra�
Base = 35; % temp zew. dobra� do w�asnych potrzeb
Freq = 0.05; % szybko�� zmiany tem zew, te� sobie dobra�

step_time = 50;
X0 = 35; % temp pocz�tkowa klimatyzatora, powinno by� to samo co w Base ale juz nie mienialem na t� sam� zmienn�, do swoich wykres�w lepiej zmieni� na inn� warto��, np. 27
X10 = 35; % temp pocz�tkowa w punkcie U1 (mo�e by� to samo co X0)
X20 = 35; % tempp pocz�tkowa w punkcjie U2 (mo�e by� to samo co X0)

Io = 15;
Rp1 = [10, 40 ,100]; % <- tu zmie� na jakie� warto�ci, powinny troch� si� od siebie r�ni� zeby mo�na by�o zaobserowoa� wp�yw
Rp2 = [40, 40, 40]; % <- tu zmie� na jakie� warto�ci, powinny troch� si� od siebie r�ni� zeby mo�na by�o zaobserowoa� wp�yw
Cp1 = [30, 40, 50]; % <- tu zmie� na jakie� warto�ci, powinny troch� si� od siebie r�ni� zeby mo�na by�o zaobserowoa� wp�yw
Cp2 = [60, 75, 90]; % <- tu zmie� na jakie� warto�ci, powinny troch� si� od siebie r�ni� zeby mo�na by�o zaobserowoa� wp�yw
for i=1:length(Rp1)
    R1 = Rp1(1); % raz tutaj daj Rp1(1) gdy badasz wp�yw C a raz daj Rp(i) gdy badasz wp�yw R ,ni�ej tak samo
    R2 = Rp2(1);
    C1 = Cp1(i);
    C2 = Cp2(i);

    sim('klim_ex', 1000);
    figure(i)
    hold on;
    plot(T, U1);
    plot(T, U2);
    legend('U1','U2','U1','U2','U1','U2');
end

%% z wykres�w wida�, �e R ma znaczenie podczas oscylacji temp zew (du�ej warto�ci Amp, np. 20)
%% wida� te�, �e wraz ze wzrostem C ro�nie inercja(?) uk�adu, uk�ad bardziej si� przeregulowuje, trudniej jest go zatrzyma� na w�a�ciwym miejscu
%% oscylacje temp zew maj� wp�yw na temp wew i r�wnie� powoduj� jej oscylacje
%% a nastawy to chyba ka�dy sobie musi wybra� �eby by�o cokolwiek innego :) 
%% og�lnie ze wzrostem parametru I b�d� sie pojawia�y coraz wi�ksze przeregulowania
%% ze wzrostem D (bardzo du�ym, np.100, wykres b�dzie si� odkszta�ca� ale w moim przypadku nie pojawi�o si� wi�cej przeregulowa�)
%% P-> ze wzrostem tego parametru ro�nie szybko�� stabilizacji systemu
