close all;
clear all;
clc;

a=[1; 3; 5; 2; 1];
E=zeros(1,1000);
F=zeros(1,1000);
E_sum=zeros(1,1000);
F_sum=zeros(1,1000);
for i=10:1000
    for j=1:50
        X=randn(i,5);

        Y=X*a+randn; %gdzie v ma byc jakims szumem np.bialym, szarym
       
        %%%%%%wersja1
        b=((X'*X)^(-1))*X'*Y;
        E(i)=norm(a-b);
        E_sum(i) = E_sum(i) + E(i);
        
        %%%%%%wersja2
        [U,S,V]=svd(X, 'econ');
        c=V*(S^(-1))*U'*Y;
        F(i)=norm(a-c);
        F_sum(i)=F_sum(i)+F(i);
        
    end;
    E_sum(i) = E_sum(i) / 50;
    F_sum(i) = F_sum(i) / 50;
end;

plot(E_sum)

figure(1);
plot(E_sum);
hold on;
print('wersja1','-dpng')

figure(2);
plot(F_sum);
hold on;
print('wersja2','-dpng')

figure(3);
plot(E_sum);
hold on;
plot(F_sum);
hold on;
print('wersja12','-dpng')