step=10;
a1=2;
a2=3;
sample_time=0.5;
s=tf ('s');
sim('frezarka_sim'); % s*(s^2+sa2+sa1+a1a2) = s^3+s^2(a1+a2)+s(a1a2)+0

t=time;
d=data;
r_d=r_data;

t_dis=time_dis;
d_dis=data_dis;
r_d_dis=r_data_dis;

figure(1);

subplot(2,1,1);
plot(t,r_d,'.r-');
grid on;
title('Regulator ci�g�y:');
xlabel('Czas [s]');
ylabel('Wartosc');

subplot(2,1,2);
plot(t,d,'.b-');
grid on;
title('Obiekt:');
xlabel('Czas [s]');
ylabel('Wartosc');

%dorobic dyskretny, ponoc tylko bloczek mozesz zmienic
figure(2);

subplot(2,1,1);
plot(t_dis,r_d_dis,'.r-');
grid on;
title('Regulator dyskretny:');
xlabel('Czas [s]');
ylabel('Wartosc');

subplot(2,1,2);
plot(t_dis,d_dis,'.b-');
grid on;
title('Obiekt:');
xlabel('Czas [s]');
ylabel('Wartosc');

